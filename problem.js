const products = [
     {
          shampoo: {
               price: "$50",
               quantity: 4,
          },
          "Hair-oil": {
               price: "$40",
               quantity: 2,
               sealed: true,
          },
          comb: {
               price: "$12",
               quantity: 1,
          },
          utensils: [
               {
                    spoons: { quantity: 2, price: "$8" },
               },
               {
                    glasses: { quantity: 1, price: "$70", type: "fragile" },
               },
               {
                    cooker: { quantity: 4, price: "$900" },
               },
          ],
          watch: {
               price: "$800",
               quantity: 1,
               type: "fragile",
          },
     },
];

let newobject = Object.entries(products[0]);
let result = newobject.filter((value) => {
     if (Array.isArray(value[1])) {
          let resultvalue = value[1].filter((insidevalue) => {
               let newobjectvalue = Object.entries(insidevalue);
               let finalvalue = newobjectvalue[0][1].price.replace("$", "");
               if (finalvalue > 65) {
                    console.log(insidevalue);
               }
          });
     } else {
          let comparePrice = value[1].price.replace("$", "");
          if (comparePrice > 65) {
               return comparePrice;
          }
     }
});
console.log(result);

let quantityresult = newobject.filter((value) => {
     if (Array.isArray(value[1])) {
          let resultvalue = value[1].filter((insidevalue) => {
               let newobjectvalue = Object.entries(insidevalue);
               if (newobjectvalue[0][1].quantity > 1) {
                    console.log(insidevalue);
               }
          });
     } else {
          let comparequantity = value[1].quantity;
          if (comparequantity > 1) {
               return comparequantity;
          }
     }
});
console.log(quantityresult);

let frugile = newobject.filter((value) => {
     if (Array.isArray(value[1])) {
          let resultvalue = value[1].filter((insidevalue) => {
               let newobjectvalue = Object.entries(insidevalue);
               if (newobjectvalue[0][1].type === "fragile") {
                    console.log(newobjectvalue);
               }
          });
     } else {
          if (value[1].type === "fragile") {
               return value;
          }
     }
});
console.log(frugile);
